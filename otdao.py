import customtools


def get_po_header(conn, po, username):
    cursor = conn.cursor()
    sql = ("SELECT * FROM alt_mve_receive.[dbo].[fn_getPOHeader](?,?)")
    res = cursor.execute(sql, (po, username))
    data = cursor.fetchall()
    query_results = customtools.results_To_dict(cursor, data)

    return query_results


def check_po_status(conn, po):
    cursor = conn.cursor()
    sql = ("SELECT alt_mve_receive.[dbo].[fn_isPoOpen](?)")
    res = cursor.execute(sql, (po))
    data = cursor.fetchone()
    # query_results = customtools.results_To_dict(cursor,data)

    return data


def is_po_already_received(conn, po):
    cursor = conn.cursor()
    sql = ("SELECT alt_mve_receive.[dbo].[fn_isPoAlreadyReceived](?)")
    res = cursor.execute(sql, (po))
    data = cursor.fetchone()
    # query_results = customtools.results_To_dict(cursor,data)

    return data


def isValidLogin(conn, username, password):
    cursor = conn.cursor()
    sqlCommand = ("SELECT alt_mve_receive.dbo.fn_isValidLogin (?,?)")
    results = cursor.execute(sqlCommand, [username, password])
    data = cursor.fetchone()
    return data[0]


def get_po_items(conn, po):
    cursor = conn.cursor()
    sql = ("SELECT * FROM alt_mve_receive.[dbo].[fn_getPOItems](?)")
    res = cursor.execute(sql, (po))
    data = cursor.fetchall()
    query_results = customtools.results_To_dict(cursor, data)

    return query_results


def get_po_itemsreceive(conn, po):
    cursor = conn.cursor()
    sql = ("SELECT * FROM alt_mve_receive.[dbo].[fn_getPOItemsReceived](?)")
    res = cursor.execute(sql, (po))
    data = cursor.fetchall()
    query_results = customtools.results_To_dict(cursor, data)

    return query_results


def insert_receive_items_via_po(conn, poid, username, receiveid):
    sql = 'SET XACT_ABORT ON EXEC alt_mve_receive.[dbo].[usp_insertReceiveItemsViaPO] @poid=?, @username=?,@inventory_receiveid=?'
    params = (poid)
    conn.cursor().execute(sql, poid, username, receiveid).commit()


def create_receive_header(conn, receivetype, supplierid, locationid, username,invoiceno):
    cursor = conn.cursor()
    sql = (
        "DECLARE @v int exec alt_mve_receive.[dbo].[usp_insertReceiveHeader] @receive_type=?,@supplierid=?,@locationid=?,@employee_name=?,@invoiceno=?,@receiveid = @v output")
    res = cursor.execute(sql, (receivetype, supplierid, locationid, username,invoiceno))
    data = cursor.fetchone()
    return data[0]


def get_receive_header(conn, receiveid):
    cursor = conn.cursor()
    sql = ("SELECT * FROM alt_mve_receive.[dbo].[fn_getReceiveHeader](?)")
    res = cursor.execute(sql, (receiveid))
    data = cursor.fetchall()
    query_results = customtools.results_To_dict(cursor, data)

    return query_results

def check_if_contact_lens_po (conn, purchase_order_id):
    cursor = conn.cursor()
    sqlCommand = ("SELECT alt_mve_receive.dbo.[fn_isContactLensPO] (?)")
    results = cursor.execute(sqlCommand, [purchase_order_id])
    data = cursor.fetchone()
    return data[0]

def update_inventory_transaction_qty(conn, id, newquantity):
    sql = 'SET XACT_ABORT ON EXEC alt_mve_receive.[dbo].[usp_updateInventoryTransactionQuantitybyID] @id=?, @newQuantity=? SET XACT_ABORT OFF'
    params = (id, newquantity)
    conn.cursor().execute(sql, id, newquantity).commit()

def update_po_status(conn, inventory_transaction_id):
    sql = 'SET XACT_ABORT ON EXEC alt_mve_receive.[dbo].[usp_autoSetPOStatus] @inventory_transaction_id=? SET XACT_ABORT OFF'
    conn.cursor().execute(sql, inventory_transaction_id).commit()

def update_inventory_transaction_cost(conn, id, newcost):
    sql = 'SET XACT_ABORT ON EXEC alt_mve_receive.[dbo].[usp_updateInventoryTransactionCostbyID] @id=?, @newCost=? SET XACT_ABORT OFF'
    params = (id, newcost)
    conn.cursor().execute(sql, id, newcost).commit()

def update_inventory_transaction_invoice(conn, id, newInvoice):
    sql = 'SET XACT_ABORT ON EXEC alt_mve_receive.[dbo].[usp_updateInventoryTransactionInvoicebyID] @id=?, @newInvoice=? SET XACT_ABORT OFF'
    conn.cursor().execute(sql, id, newInvoice).commit()

def is_receive_id_valid(conn, id):
    cursor = conn.cursor()
    sqlCommand = ("SELECT alt_mve_receive.dbo.[fn_isReceiveIDValid](?)")
    results = cursor.execute(sqlCommand, [id])
    data = cursor.fetchone()
    return data[0]

def is_contact_lens_upc(conn, upc):
    cursor = conn.cursor()
    sqlCommand = ("SELECT alt_mve_receive.dbo.[fn_isUPCAndSupplierValid](?)")
    results = cursor.execute(sqlCommand, upc)
    data = cursor.fetchone()
    return data[0]


def get_po_itemsreceive_by_receiveid(conn, receiveid):
    cursor = conn.cursor()
    sql = ("SELECT * FROM alt_mve_receive.[dbo].[fn_getPOReceivedItemsbyReceiveID](?)")
    res = cursor.execute(sql, (receiveid))
    data = cursor.fetchall()
    query_results = customtools.results_To_dict(cursor, data)

    return query_results


def insert_contactlens_item(conn, upc, qty, locationid, supplierid, username, receiveid):
    sql = 'EXEC alt_mve_receive.[dbo].[usp_insertContactLensIntoReceive] @upc=?, @quantity=?, @locationid=?, @supplierid=?, @username=?, @inventory_receiveid=?'
    conn.cursor().execute(sql, upc, qty,locationid,supplierid,username,receiveid).commit()


