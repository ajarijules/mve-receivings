from flask import Flask, abort, flash, redirect, render_template, request, url_for, session, Response, \
    copy_current_request_context
import config as cfg
import pyodbc
import otdao

app = Flask(__name__)
app = Flask(__name__, static_url_path="/static")

connection = pyodbc.connect(cfg.connectionString)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
@app.route('/')
def index():
    error = None
    if request.method == 'GET':
        try:
            if session['isAuthenticated']:

                if session['isAuthenticated'] == True:

                    return render_template("index.html",)
                else:
                    error = "Please Log In !"
                    return redirect(url_for('login', ))
            else:
                session['isAuthenticated'] = False
                error = "Please Log In !"
                return redirect(url_for('login', ))
        except:
            session['isAuthenticated'] = False
            return redirect(url_for('login', ))
    return render_template("index.html")


@app.route("/searchpo", methods=['GET', 'POST'])
def search_po():
    poheader=['']
    po=''
    errors = []
    if session['isAuthenticated'] == False:
        error = "Please Log In !"
        return redirect(url_for('login', ))
    else:
        if request.method == 'POST':
            if 'posearch' in request.form:
                po = request.form['poSearch']
                #check if po closed
                po_status_check = otdao.check_po_status(connection,po)
                print(po_status_check[0])
                if po_status_check[0] == True:
                    poheader = otdao.get_po_header(connection, po,session['username'])
                    poitems = otdao.get_po_items(connection,po)

                    return render_template("searchpo.html", poheader=poheader, poitems=poitems)
                else:
                    error = 'Purchase order is closed'
                    errors.append(error)
                    print(errors)
                    return render_template("searchpo.html", poheader=poheader,poerrors=errors)

    return render_template("searchpo.html",poheader=poheader,lastsearchedPo = po,poerrors=[])

@app.route("/receivepo", methods=['GET', 'POST'])
def receive_po():
    poheader=['']
    po=''
    errors = []
    if session['isAuthenticated'] == False:
        error = "Please Log In !"
        return redirect(url_for('login', ))
    else:
        if request.method == 'POST':
            if 'receivePo' in request.form:
                po = request.form['poSearch']
                po_status_check = otdao.check_po_status(connection,po)
                #check if po closed and po already received
                #po_status_check = otdao.check_po_status(connection,po)
                is_po_already_received = otdao.is_po_already_received(connection,po)
                print(is_po_already_received[0])

                     #True = Open
                if po_status_check[0] == False:
                    error = 'Purchase order is closed'
                    errors.append(error)
                    print(errors)
                    print(po_status_check[0])


                    #0 = Not received yet
                    if is_po_already_received[0] > 0:
                        print('received')
                        poheader = otdao.get_po_header(connection, po, session['username'])
                        poitems = otdao.get_po_itemsreceive(connection, po)
                        return render_template("receiveviapo.html", poheader=poheader, poitems=poitems , poerrors=errors)


                if po_status_check[0] == True:
                    print('open')
                    if is_po_already_received[0] == 0:
                        print('bye2')
                        # Create a receive header
                        # Add items from po to the receive header


                        poheader = otdao.get_po_header(connection, po,session['username'])
                        poitems = otdao.get_po_items(connection,po)
                        #poitems = otdao.get_po_itemsreceive(connection, po)
                        return render_template("receiveviapo.html", poheader=poheader, poitems=poitems)
             #   else:
               #     print('why')
                   # error = 'Purchase order is closed'
                  #  errors.append(error)
                   # print(errors)
                  #  return render_template("receiveviapo.html", poheader=poheader,poerrors=errors)

    return render_template("receiveviapo.html",poheader=poheader,lastsearchedPo = po,poerrors=[])

@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if not username:
            error.append("Please enter your username")
        if not password:
            error.append("Please enter your password")

        if username and password:
            session['isAuthenticated'] = otdao.isValidLogin(connection, username, password)
            if session['isAuthenticated'] == True:
                #otdao.openSession(connection, username, datetime.now())
                session['username'] = username
                #sessionid = otdao.getSessionID(connection, session['username'])
                #session['sessionid'] = sessionid
                return redirect(url_for('index', ))
            else:
                error = "Could not log in username or password is incorrect !"
                return render_template("login.html", errors=error)
    return render_template("login.html", errors=error)

@app.route("/logout", methods=['GET'])
def logout():
    if request.method == 'GET':
        #otdao.closeSession(connection, session['sessionid'], datetime.now())
        session['username'] = None
        session['isAuthenticated'] = False
        session['sessionid'] = None
    return redirect(url_for('login', ))
if __name__ == '__main__':
    app.run(debug=True, )
