def results_To_dict(cursor,data):
    query_results = [dict(zip([column[0] for column in cursor.description], row)) for row in data]
    return query_results