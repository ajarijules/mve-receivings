from flask import Flask, abort, flash, redirect, render_template, request, url_for, session, Response, \
    copy_current_request_context
import config as cfg
import pyodbc
import otdao
import datetime
from flask import jsonify
import simplejson as json
import pypyodbc

app = Flask(__name__)
app = Flask(__name__, static_url_path="/static")

connection = pyodbc.connect(cfg.connectionString, autocommit=True)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
currentreceiveid = None

@app.route('/')
def index():
    error = None
    if request.method == 'GET':
        try:
            if session['isAuthenticated']:

                if session['isAuthenticated'] == True:

                    return render_template("index.html", )
                else:
                    error = "Please Log In !"
                    return redirect(url_for('login', ))
            else:
                session['isAuthenticated'] = False
                error = "Please Log In !"
                return redirect(url_for('login', ))
        except:
            session['isAuthenticated'] = False
            return redirect(url_for('login', ))
    return render_template("index.html")


@app.route("/searchpo", methods=['GET', 'POST'])
def search_po():
    poheader = ['']
    po = ''
    errors = []
    if session['isAuthenticated'] == False:
        error = "Please Log In !"
        return redirect(url_for('login', ))
    else:
        if request.method == 'POST':
            if 'posearch' in request.form:
                po = request.form['poSearch']
                # check if po closed
                po_status_check = otdao.check_po_status(connection, po)
                ##print(po_status_check[0])

                if po_status_check[0] == True:
                    poheader = otdao.get_po_header(connection, po, session['username'])
                    poitems = otdao.get_po_items(connection, po)

                    return render_template("searchpo.html", poheader=poheader, poitems=poitems)
                else:
                    error = 'Purchase order is closed'
                    errors.append(error)
                    ##print(errors)
                    return render_template("searchpo.html", poheader=poheader, poerrors=errors)

    return render_template("searchpo.html", poheader=poheader, lastsearchedPo=po, poerrors=[])


@app.route("/receiveitems", methods=['GET', 'POST'])
def receive_items():
    poheader = ['']
    receiveheaderinfo = ['']
    po = ''
    errors = []
    create_header_data = {'locations': [{'id': 15, 'name': 'Frederick Street'},
                                        {'id': 18, 'name': 'Head Office'},
                                        {'id': 19, 'name': 'Grand Bazaar'},
                                        {'id': 20, 'name': 'Chaguanus'},
                                        {'id': 21, 'name': 'Gulf City'},
                                        {'id': 22, 'name': 'West Mall'},
                                        {'id': 23, 'name': 'Trincity'},
                                        {'id': 24, 'name': 'Arima'},
                                        {'id': 25, 'name': 'Price Plaza'},
                                        {'id': 26, 'name': 'C3 Centre'},
                                        {'id': 27, 'name': 'Pennywise'}
                                        ],
                          'suppliers': [{'id': 157, 'name': 'Vistakon'}, {'id': 156, 'name': 'ABB Optical'},
                                        {'id': 285, 'name': 'Today'"'"'s Optical Limited (Stock Room)'}],
                          'employee_name': 'Ajari Jules',
                          'receivedate': datetime.datetime.now().strftime("%Y-%m-%d")

                          }
    if session['isAuthenticated'] == False:
        error = "Please Log In !"
        return redirect(url_for('login', ))
    else:
        if request.method == 'POST':
            if 'openReceive' in request.form:
                receiveid = request.form['receiveidSearch']
                receiveheader, items = open_receive_via_id(receiveid)
                if receiveheader and items:
                    temp_header_ifo = {'locationid': receiveheader[0]['locationid'],
                                       'supplierid': receiveheader[0]['supplierid'],
                                       'username': receiveheader[0]['employee_name'],
                                       'receive_id': receiveheader[0]['receiveid']}
                    session['current_receive'] = temp_header_ifo
                    return render_template('receiveviaupc.html', receiveheaderinfo=receiveheader, receiveitems=items)
                else:
                    error = f'Please enter a valid Receive ID. Make sure that the Receive ID exists and it has only Contact Lens line items.'
                    errors.append(error)
                    return render_template("receiveviaupc.html",errors=errors,
                                           create_header_data=create_header_data)
            if 'addItem' in request.form:
                upccode = request.form['upccode']
                quantity = request.form['quantity']


                if upccode and quantity:
                    check_if_contact_lens_upc = otdao.is_contact_lens_upc(connection, upccode)
                    if check_if_contact_lens_upc == True:

                        otdao.insert_contactlens_item(connection, upccode, quantity, session['current_receive']['locationid'],
                                                      session['current_receive']['supplierid'],
                                                      session['current_receive']['username'],
                                                      session['current_receive']['receive_id'])
                        receiveheader, items = open_receive_via_id(session['current_receive']['receive_id'])

                        return render_template('receiveviaupc.html', receiveheaderinfo=receiveheader, receiveitems=items)
                    else:
                        receiveheader, items = open_receive_via_id(session['current_receive']['receive_id'])
                        error = 'Please Enter a valid Contact Lens UPC '
                        errors.append(error)
                        ##print(errors)
                        return render_template('receiveviaupc.html', receiveheaderinfo=receiveheader,
                                               receiveitems=items, errors=errors)
                else:
                    receiveheader, items = open_receive_via_id(session['current_receive']['receive_id'])
                    error = 'Please Enter a UPC  and Quantity'
                    errors.append(error)
                    ##print(errors)
                    return render_template('receiveviaupc.html', receiveheaderinfo=receiveheader, receiveitems=items, errors=errors)

            if 'createReceiveHeader' in request.form:
                location = request.form['location']
                supplier = request.form['supplier']
                invoice_no = request.form['new_invoice_no']
                if location and supplier:
                    username = request.form['username']
                    receive_date = request.form['receive_date']
                    newreceiveid = otdao.create_receive_header(connection,'Receive',supplier,location,username,invoice_no)
                    receiveheader, items = open_receive_via_id(newreceiveid)
                    temp_header_ifo = {'locationid': receiveheader[0]['locationid'],
                                       'supplierid': receiveheader[0]['supplierid'],
                                       'username': receiveheader[0]['employee_name'],
                                       'receive_id': receiveheader[0]['receiveid']}
                    session['current_receive'] = temp_header_ifo
                    ##print(location,supplier,username,receive_date)
                    return render_template('receiveviaupc.html', receiveheaderinfo=receiveheader,)
                else:
                    error = 'Please Enter a Location and Supplier'
                    errors.append(error)
                    ##print(errors)
                    return render_template("receiveviaupc.html",create_header_data=create_header_data,errors=errors)



    return render_template("receiveviaupc.html",create_header_data=create_header_data)


@app.route("/receivepo", methods=['GET', 'POST'])
def receive_po():
    poheader = ['']
    receiveheaderinfo = ['']
    po = ''
    errors = []
    #session['currentReceive'] = Non
    global currentreceiveid
    if session['isAuthenticated'] == False:
        error = "Please Log In !"
        return redirect(url_for('login', ))
    else:
        if request.method == 'POST':
            if 'addPOToReceive' in request.form:
                po = request.form['addPo_poSearch']
                po_status_check = otdao.check_po_status(connection, po)
                is_po_already_received = otdao.is_po_already_received(connection, po)
                contact_lens_po = otdao.check_if_contact_lens_po(connection, po)
                ##print(f'Contact Lens Check retuned {contact_lens_po}')
                if contact_lens_po == False:
                    error = 'Please choose a Purchase Order with only Contact Lenses'
                    errors.append(error)
                    receiveheaderinfo = otdao.get_receive_header(connection,
                                                                 session['currentReceive'])
                    poitems = otdao.get_po_itemsreceive_by_receiveid(connection, session['currentReceive'])
                    return render_template("receiveviapo.html", poerrors=errors,
                                           receiveheaderinfo=receiveheaderinfo,poheader=poheader, poitems=poitems)
                else:
                    if po_status_check[0] == False:
                        if is_po_already_received[0] > 0:
                            error = 'This Purchase order is closed, and was already received using the receive id {}'.format(
                                is_po_already_received[0])
                            errors.append(error)
                            return render_template("receiveviapo.html", poerrors=errors,
                                                   receiveheaderinfo=receiveheaderinfo)
                        else:
                            error = 'This Purchase order is closed, but was not received. We still can not create a receive for already closed PO'
                            errors.append(error)
                            return render_template("receiveviapo.html", poerrors=errors,
                                                   receiveheaderinfo=receiveheaderinfo)
                    else:  # Po is Open
                        #
                        if is_po_already_received[0] > 0:
                            #print('received')
                            error = 'This PO is open but was already received. Please check Receive ID {}'.format(
                                is_po_already_received[0])
                            errors.append(error)
                            return render_template("receiveviapo.html", poerrors=errors,
                                                   receiveheaderinfo=receiveheaderinfo)
                        else:
                            #Since po is open and was not received we must add po to current receive
                            #Get Po Items and insert int Receive
                            receiveheaderinfo = otdao.get_receive_header(connection,
                                                                         session['currentReceive'] )
                            otdao.insert_receive_items_via_po(connection, po, session['username'], session['currentReceive'])
                            #print("Items inserted for Receive {} from PO {}".format(session['currentReceive'], po))
                            poitems = otdao.get_po_itemsreceive_by_receiveid(connection, session['currentReceive'])
                            # poitems = otdao.get_po_itemsreceive(connection, po)

                            return render_template("receiveviapo.html", poheader=poheader, poitems=poitems,
                                                   receiveheaderinfo=receiveheaderinfo)

            if 'createReceiveViaPO' in request.form:
                po = request.form['poSearch']
                invoice_no =  request.form['newinvoiceno']
                po_status_check = otdao.check_po_status(connection, po)
                contact_lens_po = otdao.check_if_contact_lens_po(connection,po)
                # check if po closed and po already received
                # po_status_check = otdao.check_po_status(connection,po)
                is_po_already_received = otdao.is_po_already_received(connection, po)
                #print(is_po_already_received[0])
                #print(f'Contact Lens Check retuned {contact_lens_po}')
                # If po is closed we need to do nothing but show error, you can not receive a closed po
                if contact_lens_po == False:
                    error = 'Please choose a Purchase Order with only Contact Lenses'
                    errors.append(error)
                    return render_template("receiveviapo.html", poerrors=errors,
                                           receiveheaderinfo=receiveheaderinfo)
                else:
                    if po_status_check[0] == False:
                        if is_po_already_received[0] > 0:
                            error = 'This Purchase order is closed, and was already received using the receive id {}'.format(
                                is_po_already_received[0])
                            errors.append(error)
                            return render_template("receiveviapo.html", poerrors=errors,
                                                   receiveheaderinfo=receiveheaderinfo)
                        else:
                            error = 'This Purchase order is closed, but was not received. We still can not create a receive for already closed PO'
                            errors.append(error)
                            return render_template("receiveviapo.html", poerrors=errors,
                                                   receiveheaderinfo=receiveheaderinfo)
                    else:  # Po is Open
                        #
                        if is_po_already_received[0] > 0:
                            #print('received')
                            error = 'This PO is open but was already received. Please check Receive ID {}'.format(
                                is_po_already_received[0])
                            errors.append(error)
                            return render_template("receiveviapo.html", poerrors=errors,
                                                   receiveheaderinfo=receiveheaderinfo)
                        else:
                            # Since po is open and was not received we must create a receive header and import lineitems
                            poheader = otdao.get_po_header(connection, po, session['username'])
                            #poheader_json = jsonify(poheader)
                            ##print(poheader_json)
                            new_receive_header = otdao.create_receive_header(connection, 'Receive',
                                                                             poheader[0]['supplierid'],
                                                                             poheader[0]['locationid'], session['username'],invoice_no)
                            #print(new_receive_header)  # <-- new header created and id is stored in variable
                            #session['currentReceive'] = new_receive_header
                            currentreceiveid = new_receive_header
                            session['currentReceive'] = new_receive_header
                            receiveheaderinfo = otdao.get_receive_header(connection, new_receive_header)
                            otdao.insert_receive_items_via_po(connection, po, session['username'], new_receive_header)
                            #print("Items inserted for Receive {} from PO {}".format(new_receive_header, po))
                            #poitems = otdao.get_po_itemsreceive(connection, po)
                            poitems = otdao.get_po_itemsreceive_by_receiveid(connection, session['currentReceive'])
                            #print(poitems)
                            # poitems = otdao.get_po_itemsreceive(connection, po)
                            return render_template("receiveviapo.html",lastsearchedPo=po, poerrors=[],poitems=poitems,poheader=poheader,
                                                   receiveheaderinfo=receiveheaderinfo)
            if 'openReceive' in request.form:
                receiveid = request.form['receiveidSearch']
                receive_id_check = otdao.is_receive_id_valid(connection, receiveid)  # check if receive id exists
                #print(receive_id_check)
                if receive_id_check == True:
                    session['currentReceive'] = receiveid
                    #print(session['currentReceive'])
                    receiveheaderinfo = otdao.get_receive_header(connection,
                                                                 receiveid)  # get header items and load the receive
                    poitems = otdao.get_po_itemsreceive_by_receiveid(connection, receiveid)

                    return render_template("receiveviapo.html", poheader=['', ''], poitems=poitems,
                                           receiveheaderinfo=receiveheaderinfo)
                else:
                    error = f'Please enter a valid Receive ID. Make sure that the Receive ID exists and it has only Contact Lens line items.'
                    errors.append(error)
                    return render_template("receiveviapo.html", poerrors=errors,
                                           receiveheaderinfo=receiveheaderinfo)

                return render_template("receiveviapo.html", receiveheaderinfo=receiveheaderinfo)
    return render_template("receiveviapo.html", poheader=poheader, lastsearchedPo=po, poerrors=[],
                           receiveheaderinfo=receiveheaderinfo)


def open_receive_via_id(id):
    # receiveid = request.form['receiveidSearch']
    receive_id_check = otdao.is_receive_id_valid(connection, id)  # check if receive id exists
    #print(receive_id_check)
    if receive_id_check == True:
        receiveheaderinfo = otdao.get_receive_header(connection, id)  # get header items and load the receive
        items = otdao.get_po_itemsreceive_by_receiveid(connection, id)
        return receiveheaderinfo, items
    else:
        return None, None


@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if not username:
            error.append("Please enter your username")
        if not password:
            error.append("Please enter your password")

        if username and password:
            session['isAuthenticated'] = otdao.isValidLogin(connection, username, password)
            if session['isAuthenticated'] == True:
                # otdao.openSession(connection, username, datetime.now())
                session['username'] = username
                # sessionid = otdao.getSessionID(connection, session['username'])
                # session['sessionid'] = sessionid
                return redirect(url_for('index', ))
            else:
                error = "Could not log in username or password is incorrect !"
                return render_template("login.html", errors=error)
    return render_template("login.html", errors=error)


@app.route('/updateInventoryTransactionQty', methods=['POST'])
def updateInventoryTransactionQty():
    newQty = request.form['newQty'];
    selected_ID = request.form['inventoryTransactionID'];
    #print(newQty)
    #print(selected_ID)
    # newlensname = [lenses[1] for lenses in all_contacts if selected_manu in lenses]
    otdao.update_inventory_transaction_qty(connection, selected_ID, newQty)
    otdao.update_po_status(connection, selected_ID)
    #print("success")
    return "success";

@app.route('/updateInventoryTransactionCost', methods=['POST'])
def updateInventoryTransactionCost():
    newCost = request.form['newCost'];
    selected_ID = request.form['inventoryTransactionID'];
    #print(newCost)
    #print(selected_ID)
    # newlensname = [lenses[1] for lenses in all_contacts if selected_manu in lenses]
    otdao.update_inventory_transaction_cost(connection, selected_ID, newCost)
    #print("success")
    return "success";

@app.route('/updateInventoryTransactionInvoice', methods=['POST'])
def updateInventoryTransactionInvoice():
    newInvoice = request.form['newInvoice'];
    selected_ID = request.form['inventoryTransactionID'];
    #print(newCost)
    #print(selected_ID)
    # newlensname = [lenses[1] for lenses in all_contacts if selected_manu in lenses]
    try:
        otdao.update_inventory_transaction_invoice(connection, selected_ID, newInvoice)
    except:
        return "Error"
    #print("success")
    return "success";

@app.route("/logout", methods=['GET'])
def logout():
    if request.method == 'GET':
        # otdao.closeSession(connection, session['sessionid'], datetime.now())
        session['username'] = None
        session['isAuthenticated'] = False
        session['sessionid'] = None
    return redirect(url_for('login', ))


if __name__ == '__main__':
    app.run(debug=True, )
